package com.piggymetrics.statistics.client;

import com.piggymetrics.statistics.domain.timeseries.DataPoint;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "statistics-mongo-service")
public interface StatisticsMongoServiceClient {

  @RequestMapping(path = "/statisticsMongo/{name}", method = RequestMethod.GET)
  List<DataPoint> findByAccountName(@PathVariable("name") String name,
                                    @RequestHeader(name="URL")String url,
                                    @RequestHeader(name="SERVICENAME")String serviceName,
                                    @RequestHeader(name="REQUESTID")String requestId,
                                    @RequestHeader(name="STAGE")String stage,
                                    @RequestHeader(name="CALLID")String callId);

  @RequestMapping(method = RequestMethod.PUT, value = "/statisticsMongo/", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
  String save(DataPoint dataPoint,
              @RequestHeader(name="URL")String url,
              @RequestHeader(name="SERVICENAME")String serviceName,
              @RequestHeader(name="REQUESTID")String requestId,
              @RequestHeader(name="STAGE")String stage,
              @RequestHeader(name="CALLID")String callId);
}
