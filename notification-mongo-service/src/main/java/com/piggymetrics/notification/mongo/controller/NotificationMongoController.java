package com.piggymetrics.notification.mongo.controller;

import com.piggymetrics.notification.mongo.domain.Recipient;
import com.piggymetrics.notification.mongo.service.NotificationMongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class NotificationMongoController {

  @Autowired
  private NotificationMongoService notificationMongoService;

  @PreAuthorize("#oauth2.hasScope('server') or #accountName.equals('demo')")
  @RequestMapping(path = "/{accountName}", method = RequestMethod.GET)
  public Recipient findByAccountName(@PathVariable String accountName) {
    return notificationMongoService.findByAccountName(accountName);
  }

  @PreAuthorize("#oauth2.hasScope('server')")
  @RequestMapping(value = "/", method = RequestMethod.PUT)
  public String save(@Valid @RequestBody Recipient recipient){
    return notificationMongoService.save( recipient);
  }

  @PreAuthorize("#oauth2.hasScope('server')")
  @RequestMapping(path = "/backup", method = RequestMethod.GET)
  public List<Recipient> findReadyForBackup() {
    return notificationMongoService.findReadyForBackup();
  }

  @PreAuthorize("#oauth2.hasScope('server')")
  @RequestMapping(path = "/remind", method = RequestMethod.GET)
  public List<Recipient> findReadyForRemind() {
    return notificationMongoService.findReadyForRemind();
  }
}
