package com.piggymetrics.notification.mongo.service;

import com.gaea.tracker.ServiceTracker;
import com.piggymetrics.notification.mongo.domain.Recipient;
import com.piggymetrics.notification.mongo.repository.RecipientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Service
public class NotificationMongoService {
  ThreadLocal<ServiceTracker> trackerThreadLocal = new ThreadLocal<ServiceTracker>();

  @Autowired
  private RecipientRepository recipientRepository;

  public String save(Recipient recipient){
    if ( trackerThreadLocal.get() == null ){
      trackerThreadLocal.set(ServiceTracker.newInstance("notification-mongo-service"));
    }
    ServiceTracker tracker = trackerThreadLocal.get();
    HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
            .getRequestAttributes()).getRequest();
    HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder
            .getRequestAttributes()).getResponse();
    tracker.trackInEvent(request, response);

    recipientRepository.save(recipient);

    tracker.trackBackOutEvent(response);
    return "void";
  }

  public Recipient findByAccountName(String accountName) {
    if ( trackerThreadLocal.get() == null ){
      trackerThreadLocal.set(ServiceTracker.newInstance("notification-mongo-service"));
    }
    ServiceTracker tracker = trackerThreadLocal.get();
    HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
            .getRequestAttributes()).getRequest();
    HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder
            .getRequestAttributes()).getResponse();
    tracker.trackInEvent(request, response);

    Assert.hasLength(accountName);
    Recipient recipient = recipientRepository.findByAccountName(accountName);

    tracker.trackBackOutEvent(response);
    return recipient;
  }

  public List<Recipient> findReadyForBackup(){
    if ( trackerThreadLocal.get() == null ){
      trackerThreadLocal.set(ServiceTracker.newInstance("notification-mongo-service"));
    }
    ServiceTracker tracker = trackerThreadLocal.get();
    HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
            .getRequestAttributes()).getRequest();
    HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder
            .getRequestAttributes()).getResponse();
    tracker.trackInEvent(request, response);

    List<Recipient> list = recipientRepository.findReadyForBackup();

    tracker.trackBackOutEvent(response);
    return list;
  }

  public List<Recipient> findReadyForRemind(){
    if ( trackerThreadLocal.get() == null ){
      trackerThreadLocal.set(ServiceTracker.newInstance("notification-mongo-service"));
    }
    ServiceTracker tracker = trackerThreadLocal.get();
    HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
            .getRequestAttributes()).getRequest();
    HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder
            .getRequestAttributes()).getResponse();
    tracker.trackInEvent(request, response);

    List<Recipient> list = recipientRepository.findReadyForRemind();

    tracker.trackBackOutEvent(response);
    return list;
  }
}
