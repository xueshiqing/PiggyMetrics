package com.piggymetrics.statistics.mongo.controller;

import com.piggymetrics.statistics.mongo.domain.timeseries.DataPoint;
import com.piggymetrics.statistics.mongo.service.StatisticsMongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class StatisticsMongoController {

  @Autowired
  private StatisticsMongoService statisticsMongoService;

  @PreAuthorize("#oauth2.hasScope('server') or #accountName.equals('demo')")
  @RequestMapping(path = "/{accountName}", method = RequestMethod.GET)
  public List<DataPoint> findByAccountName(@PathVariable String accountName) {
    return statisticsMongoService.findByAccountName(accountName);
  }

  @PreAuthorize("#oauth2.hasScope('server')")
  @RequestMapping(value = "/", method = RequestMethod.PUT)
  public String save(@Valid @RequestBody DataPoint dataPoint){
    return statisticsMongoService.save( dataPoint);
  }
}
