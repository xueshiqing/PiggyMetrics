package com.piggymetrics.statistics.mongo.repository;


import com.piggymetrics.statistics.mongo.domain.timeseries.DataPoint;
import com.piggymetrics.statistics.mongo.domain.timeseries.DataPointId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DataPointRepository extends CrudRepository<DataPoint, DataPointId> {

	List<DataPoint> findByIdAccount(String account);

}
