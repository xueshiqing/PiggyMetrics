package com.piggymetrics.account.client;

import com.piggymetrics.account.domain.Account;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "account-mongo-service")
public interface AccountMongoServiceClient {

  @RequestMapping(path = "/accountMongo/{name}", method = RequestMethod.GET)
  Account findByName(@PathVariable("name") String name,
                     @RequestHeader(name="URL")String url,
                     @RequestHeader(name="SERVICENAME")String serviceName,
                     @RequestHeader(name="REQUESTID")String requestId,
                     @RequestHeader(name="STAGE")String stage,
                     @RequestHeader(name="CALLID")String callId);

  @RequestMapping(method = RequestMethod.PUT, value = "/accountMongo/", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
  String updateAccount(Account account,
                     @RequestHeader(name="URL")String url,
                     @RequestHeader(name="SERVICENAME")String serviceName,
                     @RequestHeader(name="REQUESTID")String requestId,
                     @RequestHeader(name="STAGE")String stage,
                     @RequestHeader(name="CALLID")String callId);
}
