package com.piggymetrics.account.client;

import com.piggymetrics.account.domain.User;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "auth-service")
public interface AuthServiceClient {

	@RequestMapping(method = RequestMethod.POST, value = "/uaa/users", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	String createUser(User user,
					@RequestHeader(name="URL")String url,
					@RequestHeader(name="SERVICENAME")String serviceName,
					@RequestHeader(name="REQUESTID")String requestId,
					@RequestHeader(name="STAGE")String stage,
					@RequestHeader(name="CALLID")String callId);

}
