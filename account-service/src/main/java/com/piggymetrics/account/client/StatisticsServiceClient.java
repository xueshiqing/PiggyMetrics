package com.piggymetrics.account.client;

import com.piggymetrics.account.domain.Account;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "statistics-service")
public interface StatisticsServiceClient {

	@RequestMapping(method = RequestMethod.PUT, value = "/statistics/{accountName}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	String updateStatistics(@PathVariable("accountName") String accountName, Account account,
							@RequestHeader(name="URL")String url,
							@RequestHeader(name="SERVICENAME")String serviceName,
							@RequestHeader(name="REQUESTID")String requestId,
							@RequestHeader(name="STAGE")String stage,
							@RequestHeader(name="CALLID")String callId);

}
