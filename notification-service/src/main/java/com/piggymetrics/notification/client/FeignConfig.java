package com.piggymetrics.notification.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gaea.tracker.ServiceTracker;
import feign.Response;
import feign.codec.Decoder;
import feign.codec.ErrorDecoder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.ResponseEntityDecoder;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.io.IOException;
import java.lang.reflect.Type;

@Configuration
public class FeignConfig {

//  @Bean
//  public RequestInterceptor feignInterceptor() {
//    return new MyInterceptor();
//  }

  @Bean
  public Decoder feignDecoder() {
    return new MyDecoder();
  }

  @Bean
  public ErrorDecoder feignErrorDecoder(){
    return new MyErrorDecoder();
  }

  /**
   * 调用http时
   */
//  public static class MyInterceptor implements RequestInterceptor {
//
//    @Override
//    public void apply(RequestTemplate template) {
//      //设置header
//      Identifier identifier = ServiceService.tracker.generateIdentifier();
//      template.header(IdentifierKey.URL.toString(), identifier.getUrl());
//      template.header(IdentifierKey.SERVICENAME.toString(), identifier.getServiceName());
//      template.header(IdentifierKey.REQUESTID.toString(), identifier.getRequestID());
//      template.header(IdentifierKey.STAGE.toString(), identifier.getStage());
//    }
//  }
//
  static class MyDecoder implements Decoder {
    Log logger = LogFactory.getLog(MyDecoder.class);

    final Decoder delegate = new Default();

    /**
     * http返回时
     */
    @Override
    public Object decode(Response response, Type type) throws IOException {
      logger.info(response.headers());

      ServiceTracker tracker = ServiceTracker.newInstance("notification-service");
      tracker.trackBackInEvent(response.headers(),response.status() + "");

      HttpMessageConverter jacksonConverter = new MappingJackson2HttpMessageConverter(new ObjectMapper());
      ObjectFactory<HttpMessageConverters> objectFactory = () -> new HttpMessageConverters(jacksonConverter);
      Decoder delegate = new ResponseEntityDecoder(new SpringDecoder(objectFactory));

      return delegate.decode(response, type);
    }
  }

  static class MyErrorDecoder implements ErrorDecoder{
    final ErrorDecoder delegate = new Default();

    @Override
    public Exception decode(String methodKey, Response response) {

      ServiceTracker tracker = ServiceTracker.newInstance("notification-service");

      tracker.trackBackInEvent(response.headers(),response.status() + "");

      return delegate.decode(methodKey,response);
    }
  }
}
