package com.piggymetrics.notification.client;

import com.piggymetrics.notification.domain.Recipient;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "notification-mongo-service")
public interface NotificationMongoServiceClient {

  @RequestMapping(method = RequestMethod.PUT, value = "/notificationMongo/", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
  String save(Recipient recipient,
              @RequestHeader(name="URL")String url,
              @RequestHeader(name="SERVICENAME")String serviceName,
              @RequestHeader(name="REQUESTID")String requestId,
              @RequestHeader(name="STAGE")String stage,
              @RequestHeader(name="CALLID")String callId);

  @RequestMapping(path = "/notificationMongo/{name}", method = RequestMethod.GET)
  Recipient findByAccountName(@PathVariable("name") String name,
                              @RequestHeader(name="URL")String url,
                              @RequestHeader(name="SERVICENAME")String serviceName,
                              @RequestHeader(name="REQUESTID")String requestId,
                              @RequestHeader(name="STAGE")String stage,
                              @RequestHeader(name="CALLID")String callId);

  @RequestMapping(path = "/notificationMongo/backup", method = RequestMethod.GET)
  List<Recipient> findByReadyForBackup(@RequestHeader(name="URL")String url,
                                       @RequestHeader(name="SERVICENAME")String serviceName,
                                       @RequestHeader(name="REQUESTID")String requestId,
                                       @RequestHeader(name="STAGE")String stage,
                                       @RequestHeader(name="CALLID")String callId);

  @RequestMapping(path = "/notificationMongo/remind", method = RequestMethod.GET)
  List<Recipient> findByReadyForRemind(@RequestHeader(name="URL")String url,
                                       @RequestHeader(name="SERVICENAME")String serviceName,
                                       @RequestHeader(name="REQUESTID")String requestId,
                                       @RequestHeader(name="STAGE")String stage,
                                       @RequestHeader(name="CALLID")String callId);
}
