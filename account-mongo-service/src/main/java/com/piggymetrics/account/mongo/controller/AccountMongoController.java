package com.piggymetrics.account.mongo.controller;

import com.piggymetrics.account.mongo.domain.Account;
import com.piggymetrics.account.mongo.service.AccountMongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AccountMongoController {
  @Autowired
  private AccountMongoService accountMongoService;

  @PreAuthorize("#oauth2.hasScope('server') or #name.equals('demo')")
  @RequestMapping(path = "/{name}", method = RequestMethod.GET)
  public Account findByName(@PathVariable String name) {
    return accountMongoService.findByName(name);
  }

  @PreAuthorize("#oauth2.hasScope('server')")
  @RequestMapping(value = "/", method = RequestMethod.PUT)
  public String saveAccount(@Valid @RequestBody Account account) {
    return accountMongoService.save(account);
  }
}
