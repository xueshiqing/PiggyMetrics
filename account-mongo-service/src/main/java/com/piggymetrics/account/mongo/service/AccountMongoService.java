package com.piggymetrics.account.mongo.service;

import com.gaea.tracker.ServiceTracker;
import com.piggymetrics.account.mongo.domain.Account;
import com.piggymetrics.account.mongo.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class AccountMongoService {
  ThreadLocal<ServiceTracker> trackerThreadLocal = new ThreadLocal<ServiceTracker>();

  @Autowired
  private AccountRepository repository;

  public Account findByName(String accountName){
    readInfo();
    if ( trackerThreadLocal.get() == null ){
      trackerThreadLocal.set(ServiceTracker.newInstance("account-mongo-service"));
    }
    ServiceTracker tracker = trackerThreadLocal.get();
    HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
            .getRequestAttributes()).getRequest();
    HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder
            .getRequestAttributes()).getResponse();
    tracker.trackInEvent(request, response);

    Account account = repository.findByName(accountName);

    tracker.trackBackOutEvent(response);
    return account;
  }

  public String save(Account account){
    readInfo();
    if ( trackerThreadLocal.get() == null ){
      trackerThreadLocal.set(ServiceTracker.newInstance("account-mongo-service"));
    }
    ServiceTracker tracker = trackerThreadLocal.get();
    HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
            .getRequestAttributes()).getRequest();
    HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder
            .getRequestAttributes()).getResponse();
    tracker.trackInEvent(request, response);

    repository.save(account);

    tracker.trackBackOutEvent(response);
    return "void";
  }

  private void readInfo(){
    try {
      Process process = null;
      List<String> processList = new ArrayList<String>();
      try {
        process = Runtime.getRuntime().exec("uptime");
        BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = "";
        while ((line = input.readLine()) != null) {
          processList.add(line);
        }
        input.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
      String[] strings = processList.get(0).split(" ");
      boolean isAverage = false;
      for (String s : strings){
        if ( isAverage ){
          long returnVal = Double.valueOf(s.substring(0, s.length() - 1)).longValue();
          Thread.sleep(returnVal / 4);
          break;
        }
        if ( s.length() > 2 && s.substring(0,s.length() - 1).equals("average") ){
          isAverage = true;
        }
      }
    }catch (InterruptedException e){
      Arrays.toString(e.getStackTrace());
    }
  }
}
