package com.piggymetrics.account.mongo.domain;

public enum TimePeriod {

	YEAR, QUARTER, MONTH, DAY, HOUR

}
