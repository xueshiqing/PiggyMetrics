package com.piggymetrics.auth.service;

import com.piggymetrics.auth.domain.User;

import java.security.Principal;

public interface UserService {

	String create(User user);
}
